var express = require('express')
var io = require('socket.io')
var http = require('http')
var helmet = require('helmet')
var cors = require('cors')
var fs = require('fs')
var path = require('path')
var { Engine, Bodies, Body, World} = require('matter-js')
var models = require('./models')

/** @typedef {object} GameOptions
 * @property {number} fps - FPS, 100/60 default
 * @property {number} port - port number,
 * @property {string} map - map [default]
 */

class GameServer {
  /**
   * 
   * @param {GameOptions} conf Server configuration
   */
  constructor(conf) {
    var default_config = {
      fps: 1000/60,
      port: 3000,
      map: 'default'
    }
    this.conf = Object.assign(default_config, conf)
    this.players = {}
    this.players_movement = {};
    this.map = null
    this.engine = null
    this.world = null
    this.angular_speed = 0.03 // 0.015
    this.player_speed = 2.5 // 1
    this.package = 0
    global.document = {createElement: function () { return { getContext: function () { return {}; } }; } };
    global.window = {};
  }

  setStatic(path) {
    this.conf.static = path
  }

  setPort(port) {
    this.conf.port = port
  }

  showServerConfig() {
    this.log(this.conf)
  }

  _init () {
    this.app.use(cors())
    this.app.use(express.static('./static'))
    this.log('Server static folder:', './static')
    this.app.use(helmet())
  }

  getMapPath () {
    return path.resolve(__dirname, '..', 'maps', this.conf.map + '.json')
  }

  loadMap (cb) {
    if (this.map) {
      return cb()
    } else {
      fs.exists(this.getMapPath(), (exists) => {
        if (!exists) return cb(new Error('Map file not founded [' + this.getMapPath() + ']'))
        fs.readFile(this.getMapPath(), (err, data) => {
          if (err) return cb(err)
          try {
            this.map = JSON.parse(data)
            return cb(null)
          } catch (e) {
            cb(e)
          }
        })
      })
    }
  }

  initMap (cb) {
    for (var i = 0; i < this.map.width; i++) {
      for (var j = 0; j < this.map.height; j++) {
        var n = this.map.static[this.map.height * j + i]
        if (n !== 0) {
          if (models.hasOwnProperty(n)) {
            var body = Bodies.fromVertices(0, 0, models[n].data, {
              isStatic: true
            })
            World.add(this.world, body)
            Body.setPosition(body, {
              x: i * this.map.tileSize - body.bounds.min.x + body.position.x + models[n].offset.x,
              y: j * this.map.tileSize - body.bounds.min.y + body.position.y + models[n].offset.y
            })
          }
        }
      }
    }
    //Creating game border
    World.add(this.world, [
      Bodies.rectangle(this.map.borderOffset, this.map.height * this.map.tileSize / 2, this.map.borderWidth, this.map.height * this.map.tileSize, {
        isStatic: true
      }),
      Bodies.rectangle(this.map.width * this.map.tileSize - this.map.borderOffset, this.map.height * this.map.tileSize / 2, this.map.borderWidth, this.map.height * this.map.tileSize, {
        isStatic: true
      }),
      Bodies.rectangle(this.map.width * this.map.tileSize / 2, this.map.borderOffset, this.map.width * this.map.tileSize, this.map.borderWidth, {
        isStatic: true
      }),
      Bodies.rectangle(this.map.width * this.map.tileSize / 2, this.map.height * this.map.tileSize - this.map.borderOffset, this.map.width * this.map.tileSize, this.map.borderWidth, {
        isStatic: true
      })
    ])

    cb(null)
  }

  playersAll () {
    var players_data = []
    Object.keys(this.players).forEach(player_id => {
      var player = this.players[player_id];
      players_data.push({
        i: player.id,
        n: player.name,
        t: player.type,
        p: player.body.position,
        a: player.body.angle,
        h: player.hp,
        w: player.wing
      })
    })
    return players_data
  }

  initGameEvents (cb) {
    this.io.on('connect', (socket) => {
      this.log('Client connected:', socket.id)
      // if map is loaded, listen to start
      socket.on('map_loaded', () => {
        socket.emit('menu')
      })
      // -
      socket.on('play', (player_info) => {
        console.log('play', player_info)
        if (!socket.already_played) {
          socket.already_played = true
          socket.emit('players_list', this.playersAll())
        }
        if (socket.player_id) {
          this.log('Player already playing:', socket.player_id)
        } else {
          socket.join('players')
          var respPoint = this.getRespawnPoint()
          var player = {
            id: socket.id,
            name: player_info.name,
            type: player_info.type,
            hp: 3,
            sail: 0,
            body: Bodies.fromVertices(respPoint.x, respPoint.y, models['ship'])
          }
          player.body.restitution = 1
          World.add(this.world, player.body)
          this.players[player.id] = player
          socket.player_id = player.id
          this.io.to('players').emit('new_player', {
            i: player.id,
            n: player.name,
            t: player.type,
            p: player.body.position,
            a: player.body.angle,
            h: player.hp,
            s: player.body.angle
          })
          socket.emit('player_id', player.id)
        }
      })
      // -
      socket.on('player_pos', (pos) => {
        if (socket.player_id) {
          this.players_movement[socket.player_id] = pos
        }
      })
      // -
      socket.on('disconnect', () => {
        if (socket.player_id) {
          if (this.players_movement.hasOwnProperty(socket.player_id)) {
            delete this.players_movement[socket.player_id]
          }
          World.remove(this.world, this.players[socket.player_id].body);
          delete this.players[socket.player_id]
          this.io.to('players').emit('remove_player', (socket.player_id))
          socket.player_id = null
        }
        this.log("Client disconnected:", socket.id);
      })      
      //send map to new connection
      socket.emit('map', this.map)
    })
    // -
    cb()
  }

  log() {
    console.log.apply(this, arguments)
  }

  getRespawnPoint() {
    var x, y
    while (true) {
      x = Math.floor(Math.random() * (this.map.width * this.map.tileSize - this.map.tileSize * 2)) + this.map.tileSize;
      y = Math.floor(Math.random() * (this.map.height * this.map.tileSize - this.map.tileSize * 2)) + this.map.tileSize;
      var xi = Math.floor(x / this.map.tileSize);
      var yi = Math.floor(y / this.map.tileSize);
      if (this.map.static[this.map.height * yi + xi] === 0) break
    }
    return { x, y }
  }

  start() {
    this.app = express()
    this.http = http.Server(this.app)
    this.io = io(this.http)
    this._init()
    this.loadMap((err)=>{
      if (err) return console.error(err)
      this.log('Map loaded.')
      this.engine = Engine.create()
      this.world = this.engine.world
      this.world.gravity.y = 0
      this.initMap(() => {
        this.log('Map initialized.')
        Engine.run(this.engine)
        this.log('Engine started.')
        this.initGameEvents(() => {
          this.http.listen(this.conf.port, this.started.bind(this))
        })
      })
    })
  }

  updatePlayersPositions () {
    Object.keys(this.players_movement).forEach(player_id => {
      if (this.players_movement.hasOwnProperty(player_id)) {
        var player = this.players[player_id]
        if (player) {
          var movement = this.players_movement[player_id]
          var m = {
            x: player.body.position.x - movement.x,
            y: player.body.position.y - movement.y
          }
          var a = Math.atan2(m.y - player.body.position.y, m.x - player.body.position.x) - Math.PI / 2
          var max = Math.PI * 2
          var da = (a - player.body.angle) % max
          var na = 2 * da % max - da
          Body.setAngularVelocity(player.body, na * this.angular_speed);
          //delete this.players_movement[player_id]
          var direction = {
            x: -this.player_speed * Math.sin(player.body.angle),
            y: this.player_speed * Math.cos(player.body.angle)
          }
          Body.setVelocity(player.body, direction)
          player.sail = na
        }
      }
    })
  }

  sendPlayersData () {
    var players_data = []
    Object.keys(this.players).forEach(player_id => {
      var player = this.players[player_id];
      players_data.push({
        i: player.id,
        p: player.body.position,
        a: player.body.angle,
        h: player.hp,
        s: player.sail,
        t: this.package++
      })
    })
    this.io.to('players').emit('players_update', players_data)
  }

  loop () {
    this.updatePlayersPositions()
    this.sendPlayersData()
  }

  started() {
    setInterval(this.loop.bind(this), this.conf.fps)
    this.log('Server started on port *:', this.conf.port)
  }
}

module.exports = GameServer