module.exports = {
    offset: {
      x: 0,
      y: 1
    },
    data: [
      {x: 0, y: 64},
      {x: 0, y: 2},
      {x: 16, y: 1},
      {x: 34, y: 2},
      {x: 48, y: 3},
      {x: 64, y: 2},
      {x: 64, y: 64}]
}