module.exports = {
    offset: {
      x: 2,
      y: 2
    },
    data: [
      {x: 2, y: 64},
      {x: 3, y: 39},
      {x: 12, y: 20},
      {x: 24, y: 15},
      {x: 58, y: 3},
      {x: 64, y: 2},
      {x: 64, y: 64}]
}