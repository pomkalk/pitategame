import * as PIXI from "pixi.js";
import * as io from "socket.io-client";
import * as Stats from "stats.js";

class Game {
  constructor () {
    PIXI.utils.skipHello();
    this.app = PIXI.autoDetectRenderer();
    this.app.view.style.position = "absolute";
    this.app.view.style.display = "block";
    this.app.view.style.left = 0;
    this.app.view.style.top = 0;
    this.app.view.style.zIndex = -1000;
    document.body.appendChild(this.app.view);
    this.stats = new Stats();
    document.body.appendChild(this.stats.dom);
    this.stats.showPanel(0);
    this.resize()
    window.onresize = this.resize.bind(this);
    this.statusPanel = document.getElementById('game-status')
    this.statusText = document.getElementById('status-text')
    this.menuPanel = document.getElementById('game-menu')
    this.startButton = document.getElementById('start-button')
    this.usernameField = document.getElementById('username')
    this.startButton.onclick = this.start.bind(this)
    this.textures = {}
    this.players = {}
    this.sail_speed = 0.1
    this.player_id = null
    this.bg = new PIXI.Container()
    this.fg = new PIXI.Container()
    this.stage = new PIXI.Container()
    this.update = () => {}
    this.initConnection()
    requestAnimationFrame(this.loop.bind(this))
  }

  hideMenu() {
    this.statusPanel.style.display = 'none'
    this.menuPanel.style.display = 'none'
  }

  showMenu () {
    this.statusPanel.style.display = 'none'
    this.menuPanel.style.display = ''
  }

  showStatus() {
    this.statusPanel.style.display = ''
    this.menuPanel.style.display = 'none'
  }

  resize () {
    this.width = window.innerWidth
    this.height = window.innerHeight
    this.center = {
      x: this.width * 0.5,
      y: this.height * 0.5
    }
    this.app.resize(this.width, this.height)
  }

  initConnection () {
    this.socket = io(GAME_SERVER)
    this.socket.on('connect', this.connected.bind(this))
    this.socket.on('disconnect', this.disconnected.bind(this))
    this.socket.on('map', this.loadMap.bind(this))
    this.socket.on('menu', this.showBg.bind(this))
    this.socket.on('players_list', this.initPlayers.bind(this))
    this.socket.on('player_id', this.followPlayer.bind(this))
    this.socket.on('new_player', this.newPlayer.bind(this))
    this.socket.on('remove_player', this.removePlayer.bind(this))
    this.socket.on('players_update', this.updatePlayers.bind(this))
  }

  updatePlayers (data) {
    data.forEach(player_info => {
      var player = this.players[player_info.i]
      if(player) {
        player.ship.rotation = player_info.a
        player.sprite.position.x = player_info.p.x
        player.sprite.position.y = player_info.p.y
        player.sail.rotation = player.sail.rotation + (player_info.s - player.sail.rotation ) * this.sail_speed
        player.small_sail.rotation = player.small_sail.rotation + (player_info.s - player.small_sail.rotation) * this.sail_speed
      }
    })
  }

  removePlayer (player_id) {
    var player = this.players[player_id]
    if (player) {
      this.fg.removeChild(player.sprite)
      delete this.players[player_id]
    }
  }

  newPlayer (player) {
    console.log('new_player', player.i)
    this.createPlayer(player)
  }

  createPlayer (player) {
    var sprite = new PIXI.Container()
    var ship = new PIXI.Container()
    var body = new PIXI.Sprite(this.textures['ship3'])
    var sail = new PIXI.Sprite(this.textures['sail3'])
    var small_sail = new PIXI.Sprite(this.textures['small_sail3'])
    var nest = new PIXI.Sprite(this.textures['nest'])
    var flag = new PIXI.Sprite(this.textures['flag'])
    var name = new PIXI.Text(player.n, { fontSize: 25, dropShadow: true, dropShadowAlpha: 0.1, dropShadowBlur: "", dropShadowColor: "#616161", fill: "white", strokeThickness: 2})
    body.anchor.set(0.5)
    sail.anchor.set(0.5, 0.3)
    small_sail.anchor.set(0.5)
    name.anchor.set(0.5)
    nest.anchor.set(0.5)
    flag.anchor.set(0, 1)
    sprite.position.set(player.p.x, player.p.y)
    sail.position.set(0, -15)
    small_sail.position.set(0, 28)
    nest.position.set(0, -35)
    flag.position.set(-3, -35)
    name.position.set(0, -70)
    ship.rotation = player.a
    this.fg.addChild(sprite)
    sprite.addChild(ship)
    ship.addChild(body)
    ship.addChild(nest)
    ship.addChild(flag)
    ship.addChild(small_sail)
    ship.addChild(sail)
    sprite.addChild(name)
    this.players[player.i] = {...player, ship, body, sail, sprite, small_sail}
  }

  followPlayer (player_id) {
    this.hideMenu()
    this.player_id = player_id
    this.stage.removeChildren()
    this.stage.addChild(this.bg)
    this.stage.addChild(this.fg)
    this.update = this.gameplay
  }

  initPlayers (players) {
    console.log('init players', players.length)
    players.forEach(player => {
      this.createPlayer(player)
    })
  }

  showBg () {
    this.update = this.preview
    this.showMenu()
  }

  loadMap (map) {
    this.map = map
    var allTiles = [].concat.apply([this.map.background], this.map.layers)
    var uniqTiles = allTiles.filter((item, index) => {
      if (item === 0) return false
      return allTiles.indexOf(item) == index
    })
    this.loader = new PIXI.loaders.Loader()
    uniqTiles.forEach(n => {
      this.loader.add(n.toString(), GAME_SERVER + '/images/tiles/tile_' + (n < 10 ? '0' + n : n) + '.png')
    })
    this.loader.add('ship3', GAME_SERVER + '/images/ship/ship3.png')
    this.loader.add('ship2', GAME_SERVER + '/images/ship/ship2.png')
    this.loader.add('ship1', GAME_SERVER + '/images/ship/ship1.png')
    this.loader.add('ship0', GAME_SERVER + '/images/ship/ship0.png')
    this.loader.add('sail3', GAME_SERVER + '/images/ship/sail3.png')
    this.loader.add('sail2', GAME_SERVER + '/images/ship/sail2.png')
    this.loader.add('sail1', GAME_SERVER + '/images/ship/sail1.png')
    this.loader.add('sail0', GAME_SERVER + '/images/ship/sail0.png')
    this.loader.add('small_sail3', GAME_SERVER + '/images/ship/small_sail3.png')
    this.loader.add('small_sail2', GAME_SERVER + '/images/ship/small_sail2.png')
    this.loader.add('small_sail1', GAME_SERVER + '/images/ship/small_sail1.png')
    this.loader.add('small_sail0', GAME_SERVER + '/images/ship/small_sail0.png')
    this.loader.add('flag', GAME_SERVER + '/images/ship/flag.png')
    this.loader.add('nest', GAME_SERVER + '/images/ship/nest.png')
    this.loader.load(() => {
      uniqTiles.forEach(n => {
        this.textures[n] = this.loader.resources[n].texture
      })
      this.textures['ship3'] = this.loader.resources['ship3'].texture
      this.textures["ship2"] = this.loader.resources["ship2"].texture;
      this.textures["ship1"] = this.loader.resources["ship1"].texture;
      this.textures["ship0"] = this.loader.resources["ship0"].texture;
      this.textures['sail3'] = this.loader.resources['sail3'].texture
      this.textures["sail2"] = this.loader.resources["sail2"].texture;
      this.textures["sail1"] = this.loader.resources["sail1"].texture;
      this.textures["sail0"] = this.loader.resources["sail0"].texture;
      this.textures['small_sail3'] = this.loader.resources['small_sail3'].texture
      this.textures["small_sail2"] = this.loader.resources["small_sail2"].texture;
      this.textures["small_sail1"] = this.loader.resources["small_sail1"].texture;
      this.textures["small_sail0"] = this.loader.resources["small_sail0"].texture;
      this.textures["flag"] = this.loader.resources["flag"].texture;
      this.textures["nest"] = this.loader.resources["nest"].texture;
      var bg = new PIXI.Container()
      for (var i = -map.width; i < map.width * 2; i++) {
        for (var j = -map.height; j < map.height * 2; j++) {
          var sprite = new PIXI.Sprite(this.textures[map.background])
          sprite.position.set(i * map.tileSize, j * map.tileSize)
          bg.addChild(sprite)
        }
      }
      this.bg.addChild(bg)
      this.map.layers.forEach(layer => {
        var lc = new PIXI.Container()
        for (var i = 0; i < map.width; i++) {
          for (var j = 0; j < map.height; j++) {
            var index = map.height * j + i
            if (layer[index] !== 0) {
              var sprite = new PIXI.Sprite(this.textures[layer[index]])
              sprite.position.set(i * map.tileSize, j * map.tileSize)
              lc.addChild(sprite)
            }
          }
        }
        this.bg.addChild(lc)
      })
      this.socket.emit('map_loaded')
    })
  }

  connected () {
    this.statusText.innerText = 'Loading...'
  }

  disconnected() {
    this.showStatus()
  }

  start () {
    this.socket.emit('play', {
      name: this.usernameField.value
    })
  }

  started () {

  }

  loop () {
    requestAnimationFrame(this.loop.bind(this))
    this.update()
  }

  preview () {
    this.bg.position.x = window.innerWidth / 2 - this.map.width * this.map.tileSize / 2
    this.bg.position.y = window.innerHeight / 2 - this.map.height * this.map.tileSize / 2
    this.app.render(this.bg)
  }

  gameplay () {
    this.stats.begin()
    var player = this.players[this.player_id]
    var f = 0.8
    this.bg.position.x = this.bg.position.x + ((-player.sprite.position.x + this.width / 2) - this.bg.position.x) * f
    this.bg.position.y = this.bg.position.y + ((-player.sprite.position.y + this.height / 2) - this.bg.position.y) * f
    this.fg.position.x = this.bg.position.x + ((-player.sprite.position.x + this.width / 2) - this.bg.position.x) * f
    this.fg.position.y = this.bg.position.y + ((-player.sprite.position.y + this.height / 2) - this.bg.position.y) * f

    var m = this.app.plugins.interaction.mouse.global
    this.socket.emit('player_pos', {
      x: this.center.x - m.x,
      y: this.center.y - m.y
    })
    this.app.render(this.stage)
    this.stats.end()
  }
}

export default Game
