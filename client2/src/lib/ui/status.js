import UiElement from './ui-element'

class Status extends UiElement {
  constructor(options) {
    super()
    var bar = this.createStatus()
    this.init(bar)
  }

  createStatus () {
    var container = this.createContainer()
    var cell = this.createCell()
    var loader = this.createLoader()
    var text = this.createText()
    this.text = text
    container.appendChild(cell)
    cell.appendChild(loader)
    cell.appendChild(text)
    return container
  }

  setText (text) {
    this.text.innerHTML = text
  }

  createText () {
    var text = document.createElement('div')
    text.style.marginTop = '15px'
    text.style.fontSize = '18px'
    return text
  }

  createLoader () {
    var loader = document.createElement('img')
    loader.setAttribute('src', GAME_SERVER + '/loader.gif')
    return loader
  }

  createCell () {
    var cell = document.createElement('div')
    cell.style.display = 'table-cell'
    cell.style.verticalAlign = 'middle'
    return cell
  }

  createContainer () {
    var container = document.createElement('div')
    container.style.height = '100%'
    container.style.width = '100%'
    container.style.textAlign = 'center'
    container.style.display = 'none'
    return container
  }

  show() {
    this.element.style.display = 'table'
  }

  hide() {
    this.element.style.display = 'none'
  }
}

export default Status
