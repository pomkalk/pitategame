class UiElement {
  constructor () {
    this._element = null
  }

  init (element) {
    this._element = element
  }
  get element () {
    return this._element
  }

  add (element) {
    this._element.appendChild(element.element)
  }
}

export default UiElement
