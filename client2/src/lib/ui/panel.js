import UiElement from './ui-element'

class Panel extends UiElement {
  constructor (options) {
    super()
    var panel = document.createElement('div')
    panel.style.position = 'absolute'
    panel.style.zIndex = 100
    panel.style.backgroundColor = '#FFFFFF'
    panel.style.width = '350px'
    panel.style.height = '250px'
    panel.style.margin = 0
    panel.style.padding = 0
    panel.style.left = '50%'
    panel.style.top = '50%'
    panel.style.transform = 'translate(-50%, -50%)'
    panel.style.borderRadius = '5px'
    this.init(panel)
  }

  show () {
    this.element.style.display = ''
  }

  hide () {
    this.element.style.display = 'none'
  }
}

export default Panel
