import UiElement from './ui-element'

class Menu extends UiElement {
  constructor(options) {
    super()
    this.startCb = function () { console.log("nat") }
    var menu = this.createMenu()
    this.init(menu)
  }

  onStart (cb) {
    this.startCb = cb
  }

  getName () {
    return this.input.value
  }

  click () {
    this.startCb()
  }

  createMenu () {
    var container = this.createContainer()
    var column = this.createColumn()
    var cell = this.createCell()
    var cell2 = this.createCell()
    var cell3 = this.createCell()
    this.input = this.createInput()
    this.type = 0
    var ships = this.createShips()
    var button = this.createButton()
    container.appendChild(column)
    column.appendChild(cell)
    column.appendChild(cell2)
    column.appendChild(cell3)
    cell.appendChild(this.input)
    cell2.appendChild(ships)
    cell3.appendChild(button)
    return container
  }

  createButton () {
    var button = document.createElement('button')
    button.addEventListener('click', this.click.bind(this))
    button.innerText = 'Start'
    return button
  }

  setType (element, value) {
    this.type = value
    element.style.border = '1px solid blue'
  }

  createShips () {
    var c = document.createElement("div")
    var s0 = document.createElement('img')
    var s1 = document.createElement('img')
    var s2 = document.createElement('img')
    var s3 = document.createElement('img')
    s0.src = GAME_SERVER + '/images/preview/0.png'
    s1.src = GAME_SERVER + '/images/preview/1.png'
    s2.src = GAME_SERVER + '/images/preview/2.png'
    s3.src = GAME_SERVER + '/images/preview/3.png'
    s0.onclick = () => this.setType(s0, 0)
    s1.onclick = () => this.setType(s1, 1)
    s2.onclick = () => this.setType(s2, 2)
    s3.onclick = () => this.setType(s3, 3)
    c.appendChild(s0)
    c.appendChild(s1)
    c.appendChild(s2)
    c.appendChild(s3)
    return c
  }

  createInput () {
    var input = document.createElement('input')
    input.style.width = '300px'
    input.style.borderRadius = '5px'
    input.style.padding = '5px'
    input.style.boxSizing = 'border-box'
    input.style.border = 'solid 1px blue'
    return input
  }

  createCell() {
    var cell = document.createElement('div')
    // cell.style.backgroundColor = 'rgba(0, 255, 0, 0.3)'
    return cell
  }

  createColumn () {
    var column = document.createElement('div')
    // column.style.backgroundColor = 'rgba(0, 0, 255, 0.3)'
    column.style.width = '300px'
    column.style.display = 'flex'
    column.style.flexDirection = 'column'
    column.style.justifyContent = 'center'
    return column
  }

  createContainer () {
    var container = document.createElement('div')
    // container.style.backgroundColor = 'rgba(255, 0, 0, 0.3)'
    container.style.height = '100%'
    container.style.width = '100%'
    container.style.display = 'none'
    container.style.justifyContent = 'center'
    return container
  }

  show() {
    this.element.style.display = 'flex'
  }

  hide() {
    this.element.style.display = 'none'
  }
}

export default Menu
