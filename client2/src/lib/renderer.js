import * as PIXI from 'pixi.js'
import Stats from 'stats.js'

class Renderer {
  constructor () {
    PIXI.utils.skipHello();
    this.app = PIXI.autoDetectRenderer({backgroundColor: 0x000000});
    this.app.view.style.position = "absolute";
    this.app.view.style.display = "block";
    this.app.view.style.left = 0;
    this.app.view.style.top = 0;
    this.app.view.style.zIndex = -1000;
    document.body.appendChild(this.app.view);
    this.stats = new Stats();
    document.body.appendChild(this.stats.dom);
    this.stats.showPanel(0);
    this.resize()
    window.onresize = this.resize.bind(this);
  }

  resize() {
    this.width = window.innerWidth
    this.height = window.innerHeight
    this.center = {
      x: this.width * 0.5,
      y: this.height * 0.5
    }
    this.app.resize(this.width, this.height)
  }

  render(view) {
    this.app.render(view)
  }

  _loop () {
    requestAnimationFrame(this._loop.bind(this))
    this.stats.begin()
    setImmediate(this.loopCb)
    this.stats.end()
  }

  loop (cb) {
    this.loopCb = cb
    requestAnimationFrame(this._loop.bind(this))
  }
}

export default Renderer
