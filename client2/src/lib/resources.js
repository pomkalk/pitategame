import * as PIXI from 'pixi.js'

class Resources {
  constructor () {
    this.loader = new PIXI.loaders.Loader()
    this.map = null
  }

  initMap (map) {
    this.map = map

    var allTiles = [].concat.apply([this.map.background], this.map.layers)
    var uniqTiles = allTiles.filter((item, index) => {
      if (item === 0) return false
      return allTiles.indexOf(item) == index
    })
    uniqTiles.forEach(n => {
      this.loader.add(n.toString(), GAME_SERVER + '/images/tiles/tile_' + (n < 10 ? '0' + n : n) + '.png')
    })
    this.loader.add('flagd', GAME_SERVER + '/images/ship/flagd.png')
    this.loader.add('flag0', GAME_SERVER + '/images/ship/flag0.png')
    this.loader.add('flag1', GAME_SERVER + '/images/ship/flag1.png')
    this.loader.add('flag2', GAME_SERVER + '/images/ship/flag2.png')
    this.loader.add('flag3', GAME_SERVER + '/images/ship/flag3.png')
    this.loader.add('nest', GAME_SERVER + '/images/ship/nest.png')
    this.loader.add('pole', GAME_SERVER + '/images/ship/pole.png')
    for (var i = 0; i <= 3; i++) {
      this.loader.add('ship' + i, GAME_SERVER + '/images/ship/ship' + i + '.png')
      for (var j = 0; j <= 3; j++) {
        this.loader.add('sail' + j + '_' + i, GAME_SERVER + '/images/ship/sail' + j + '_' + i + '.png')
        this.loader.add('small_sail' + j + '_' + i, GAME_SERVER + '/images/ship/small_sail' + j + '_' + i + '.png')
      }
    }
  }

  loadResources (cb) {
    this.loader.load(function () {
      cb()
    })
  }

  textures (name) {
    try {
      return this.loader.resources[name].texture
    } catch (e) {
      console.log(name)
    }
  }

  createMap() {
    var bg = new PIXI.Container()
    var water = new PIXI.Container()
    for (var i = -this.map.width; i < this.map.width * 2; i++) {
      for (var j = -this.map.height; j < this.map.height * 2; j++) {
        var sprite = new PIXI.Sprite(this.textures(this.map.background))
        sprite.position.set(i * this.map.tileSize, j * this.map.tileSize)
        water.addChild(sprite)
      }
    }
    bg.addChild(water)
    this.map.layers.forEach(layer => {
      var lc = new PIXI.Container()
      for (var i = 0; i < this.map.width; i++) {
        for (var j = 0; j < this.map.height; j++) {
        var index = this.map.height * j + i
        if (layer[index] !== 0) {
          var sprite = new PIXI.Sprite(this.textures(layer[index]))
          sprite.position.set(i * this.map.tileSize, j * this.map.tileSize)
          lc.addChild(sprite)
        }
      }
    }
    bg.addChild(lc)
    })
    return bg
  }
}

export default Resources