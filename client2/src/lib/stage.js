import * as PIXI from 'pixi.js'
class Stage {
  constructor(game) {
    this.game = game
  }

  get view () {
    return new PIXI.Container()
  }
}

export default Stage