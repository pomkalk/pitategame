import Panel from './ui/panel'
import Status from './ui/status'
import Menu from './ui/menu'

class UI {
  constructor () {
    this.startCb = function () {}
    this.panel = new Panel()
    this.menu = new Menu()
    this.status = new Status()
    this.append(this.panel)
    this.panel.add(this.menu)
    this.panel.add(this.status)
    this.status.setText('Connecting...')
    this.status.show()
    this.menu.onStart(this.start.bind(this))
  }

  onStart (cb) {
    this.startCb = cb
  }

  start () {
    this.startCb(this.menu.getName(), this.menu.type)
  }

  append(element) {
    document.body.appendChild(element.element)
  }

  hide () {
    this.panel.hide()
  }
}

export default UI
