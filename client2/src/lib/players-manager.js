import * as PIXI from 'pixi.js'
import Player from './player'

class PlayersManager {
  constructor (resources) {
    this.resources = resources
    this.players = {}
    this.count = 0
    this.deadTimeout = 5000
    this._player_id = null
    this.playerAddedFunc = function () {}
    this._fg = new PIXI.Container()
    this._dp = new PIXI.Container()
  }

  get fg () {
    return this._fg
  }

  get dp () {
    return this._dp
  }

  get player () {
    return this.players[this._player_id]
  }

  setPlayer (player_id) {
    this._player_id = player_id
  }

  redraw () {
    this._fg.removeChildren()
    Object.keys(this.players).forEach(player_id => {
      this._fg.addChild(this.players[player_id].sprite)
    })
  }

  init (players) {
    players.forEach(player => {
      var player = new Player(player, this.resources)
      if (!this.exists(player)) {
        this.players[player.id] = player
      }
    })
    this.redraw()
  }

  exists (player) {
    return this.players.hasOwnProperty(player.id)
  }

  add (player) {
    var player = new Player(player, this.resources)
    if (!this.exists(player)) {
      this.players[player.id] = player
      this.playerAddedFunc()
    }
    this.redraw()
  }

  removeDead(dead) {
    if (dead.alpha > 0.1) {
      console.log(dead.alpha)
      dead.alpha = dead.alpha - dead.alpha * 0.1
      setTimeout(()=>{ this.removeDead(dead) }, 50)
    } else {
      console.log('Removed')
      this._dp.removeChild(dead)
    }
  }

  remove (player_id) {
    var player = this.players[player_id]
    if (player) {
      var dead = player.getDead()
      this._dp.addChild(dead)
      setTimeout(() => {
        this.removeDead(dead)
      }, this.deadTimeout)
      delete this.players[player_id]
    }
    this.redraw()
  }

  playerAdded (cb) {
    this.playerAddedFunc = cb
  }

  update (data) {
    data.forEach(player_data => {
      var player = this.players[player_data.i]
      if (player) {
        player.update(player_data)
      }
    })
  }
}

export default PlayersManager