class Mouse {
  constructor (renderer) {
    this.renderer = renderer
  }

  get position () {
    return this.renderer.app.plugins.interaction.mouse.global
  }
}

export default Mouse
