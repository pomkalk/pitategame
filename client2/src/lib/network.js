import * as io from "socket.io-client";

class Network {
  constructor () {
    this.connectedFunc = function () {}
    this.loadMapFunc = function () {}
    this.initPlayersFunc = function () {}
    this.newPlayerFunc = function () {}
    this.removePlayerFunc = function () {}
    this.setPlayerFunc = function () {}
    this.updatePlayersFunc = function () {}
  }

  connect () {
    this.socket = io(GAME_SERVER)
    this.socket.on('connect', this._connected.bind(this))
    this.socket.on('map', this._loadMap.bind(this))
    // this.socket.on('disconnect', this.disconnected.bind(this))
    // this.socket.on('menu', this.showBg.bind(this))
    this.socket.on('players_list', this._initPlayers.bind(this))
    this.socket.on('player_id', this._setPlayer.bind(this))
    this.socket.on('new_player', this._newPlayer.bind(this))
    this.socket.on('remove_player', this._removePlayer.bind(this))
    this.socket.on('players_update', this._updatePlayers.bind(this))
  }

  move (data) {
    this.socket.emit('player_pos', data)
  }

  play (data) {
    this.socket.emit('play', data)
  }

  _removePlayer (player_id) {
    this.removePlayerFunc(player_id)
  }

  removePlayer (cb) {
    this.removePlayerFunc = cb
  }

  _updatePlayers (data) {
    this.updatePlayersFunc(data)
  }

  updatePlayers (cb) {
    this.updatePlayersFunc = cb
  }

  _setPlayer (player_id) {
    this.setPlayerFunc(player_id)
  }

  setPlayer (cb) {
    this.setPlayerFunc = cb
  }

  _newPlayer (player) {
    this.newPlayerFunc(player)
  }

  newPlayer (cb) {
    this.newPlayerFunc = cb
  }

  _initPlayers (players) {
    this.initPlayersFunc(players)
  }

  initPlayers (cb) {
    this.initPlayersFunc = cb
  }

  _connected () {
    this.connectedFunc()
  }

  _loadMap (map) {
    this.loadMapFunc(map)
  }

  connected (cb) {
    this.connectedFunc = cb
  }

  loadMap (cb) {
    this.loadMapFunc = cb
  }
}

export default Network