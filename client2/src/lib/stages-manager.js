class StageManager {
  constructor (renderer) {
    this.renderer = renderer
    this.active = new PIXI.Container()
    this.stages = {}
    this.initStages()
    requestAnimationFrame(this.loop.bind(this))
  }

  initStages () {

  }

  loop () {
    requestAnimationFrame(this.loop.bind(this))

  }
}

export default StageManager
