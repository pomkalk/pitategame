import * as PIXI from 'pixi.js'
import Renderer from "./renderer";
import UI from './ui'
import Network from './network'
import Resources from './resources'
import Stage from './stage'
import LoadingStage from '../stages/loading.stage'
import GameStage from '../stages/game.stage'
import PlayersManager from './players-manager'
import Mouse from './mouse'

class GameBase {
  constructor () {
    this.renderer = new Renderer()
    this.renderer.loop(this.loop.bind(this))
    this.stage = new Stage(this)
    this.ui = new UI()
    this.mouse = new Mouse(this.renderer)
    this.ui.onStart(this.onStart.bind(this))
    this.resources = new Resources()
    this.playersManager = new PlayersManager(this.resources)
    this.network = new Network()
    this.bindNetwork()
    this.network.connect()
  }

  bindNetwork() {
    this.network.connected(this.connected.bind(this))
    this.network.loadMap(this.loadMap.bind(this))
    this.network.initPlayers(this.initPlayers.bind(this))
    this.network.newPlayer(this.newPlayer.bind(this))
    this.network.setPlayer(this.setPlayer.bind(this))
    this.network.updatePlayers(this.updatePlayers.bind(this))
    this.network.removePlayer(this.removePlayer.bind(this))
  }

  removePlayer (player_id) {
    this.playersManager.remove(player_id)
  }

  updatePlayers (data) {
    this.playersManager.update(data)
  }

  setPlayer (player_id) {
    this.playersManager.setPlayer(player_id)
  }

  onStart (name, type) {
    this.network.play({name, type})
    this.stage = new GameStage(this)
    this.ui.hide()
  }

  connected () {
    this.ui.status.setText('Loading...')
  }

  loadMap (map) {
    this.resources.initMap(map)
    this.resources.loadResources(this.resourcesLoaded.bind(this))
  }

  newPlayer (player) {
    this.playersManager.add(player)
  }

  initPlayers (players) {
    this.playersManager.init(players)
  }

  resourcesLoaded () {
    this.stage = new LoadingStage(this)
    this.ui.status.hide()
    this.ui.menu.show()
  }

  loop () {
    this.renderer.render(this.stage.view)
  }

  log () { 
    console.log.apply(this, arguments)
  }
}

export default GameBase
