import * as PIXI from 'pixi.js'

class Player {
  constructor (data, resources) {
    this.resources = resources
    this._id = data.i
    this.name = data.n
    this.position = data.p
    this.angle = data.a
    this.sail_angle = data.s
    this.hp = data.h
    this.type = data.t

    this.sail_speed = 0.1
    this.initPlayer()
  }

  redraw () {
    this.ship.rotation = this.angle
    this.sprite.position.x = this.position.x
    this.sprite.position.y = this.position.y
    this.sail.rotation = this.sail.rotation + (this.sail_angle - this.sail.rotation) * this.sail_speed
    this.small_sail.rotation = this.small_sail.rotation + (this.sail_angle - this.small_sail.rotation) * this.sail_speed
  }

  update (data) {
    this.position = data.p
    this.angle = data.a
    this.hp = data.h
    this.sail_angle = data.s
    this.redraw()
  }

  get id () {
    return this._id
  }
  
  get sprite () {
    return this._sprite
  }

  resName(name) {
    return name+this.type + '_' + this.hp
  }

  getDead () {
    var sprite = new PIXI.Container()
    var ship = new PIXI.Container()
    var body = new PIXI.Sprite(this.resources.textures('ship0'))
    var sail = new PIXI.Sprite(this.resources.textures('sail' + this.type + '_0'))
    var small_sail = new PIXI.Sprite(this.resources.textures('small_sail'+this.type+'_0'))
    var nest = new PIXI.Sprite(this.resources.textures('nest'))
    var flag = new PIXI.Sprite(this.resources.textures('flagd'))
    body.anchor.set(0.5)
    sail.anchor.set(0.5, 0.3)
    small_sail.anchor.set(0.5)
    nest.anchor.set(0.5)
    flag.anchor.set(0, 1)
    sprite.position.set(this.position.x, this.position.y)
    sail.position.set(0, -15)
    small_sail.position.set(0, 28)
    nest.position.set(0, -35)
    flag.position.set(-3, -35)
    ship.rotation = this.angle
    sail.rotation = this.sail_angle
    small_sail.rotation = this.sail_angle

    sprite.addChild(ship)
    ship.addChild(body)
    ship.addChild(nest)
    ship.addChild(flag)
    ship.addChild(small_sail)
    ship.addChild(sail)
    return sprite
  }

  initPlayer () {
    this._sprite = new PIXI.Container()
    this.ship = new PIXI.Container()
    this.body = new PIXI.Sprite(this.resources.textures('ship' + this.hp))
    this.sail = new PIXI.Sprite(this.resources.textures(this.resName('sail')))
    this.small_sail = new PIXI.Sprite(this.resources.textures(this.resName('small_sail')))
    this.nest = new PIXI.Sprite(this.resources.textures('nest'))
    this.flag = new PIXI.Sprite(this.resources.textures('flag' + this.type))
    this.label = new PIXI.Text(this.name, {
      fontSize: 25,
      dropShadow: true,
      dropShadowAlpha: 0.1,
      dropShadowBlur: "",
      dropShadowColor: "#616161",
      fill: "white",
      strokeThickness: 2
    })

    this.body.anchor.set(0.5)
    this.sail.anchor.set(0.5, 0.3)
    this.small_sail.anchor.set(0.5)
    this.label.anchor.set(0.5)
    this.nest.anchor.set(0.5)
    this.flag.anchor.set(0, 1)
    this._sprite.position.set(this.position.x, this.position.y)
    this.sail.position.set(0, -15)
    this.small_sail.position.set(0, 28)
    this.nest.position.set(0, -35)
    this.flag.position.set(-3, -35)
    this.label.position.set(0, -70)
    this.ship.rotation = this.angle

    this._sprite.addChild(this.ship)
    this.ship.addChild(this.body)
    this.ship.addChild(this.nest)
    this.ship.addChild(this.flag)
    this.ship.addChild(this.small_sail)
    this.ship.addChild(this.sail)
    this._sprite.addChild(this.label)
  }
}

export default Player