import Stage from '../lib/stage'

class LoadingStage extends Stage {
  constructor(game) {
    super(game)
    this.bg = this.game.resources.createMap()
    this.map = this.game.resources.map
  }

  get view () {
    this.bg.position.x = window.innerWidth / 2 - this.map.width * this.map.tileSize / 2
    this.bg.position.y = window.innerHeight / 2 - this.map.height * this.map.tileSize / 2
    return this.bg
  }
}

export default LoadingStage