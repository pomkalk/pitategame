import * as PIXI from 'pixi.js'
import Stage from '../lib/stage'

class GameStage extends Stage {
  constructor(game) {
    super(game)
    this.bg = this.game.resources.createMap()
    this.pm = this.game.playersManager
    this.fg = this.pm.fg
    this.dp = this.pm.dp
    this.nw = this.game.network
    this.map = this.game.resources.map
    this.mouse = this.game.mouse
    this.width = window.innerWidth
    this.height = window.innerHeight
    this.center = {
      x: this.width * 0.5,
      y: this.height * 0.5
    }
    this.f = 0.8
    this.scene = new PIXI.Container()
    this.scene.addChild(this.bg)
    this.scene.addChild(this.dp)
    this.scene.addChild(this.fg)
  }

  sendPlayerInput () {
    var m = this.mouse.position
    this.nw.move({
      x: this.center.x - m.x,
      y: this.center.y - m.y
    })
  }

  get view() {
    var player = this.pm.player
    if (player) {
      this.scene.position.x = this.scene.position.x + ((-player.position.x + this.width / 2) - this.scene.position.x) * this.f
      this.scene.position.y = this.scene.position.y + ((-player.position.y + this.height / 2) - this.scene.position.y) * this.f
      // this.bg.position.x = this.bg.position.x + ((-player.position.x + this.width / 2) - this.bg.position.x) * f
      // this.bg.position.y = this.bg.position.y + ((-player.position.y + this.height / 2) - this.bg.position.y) * f
      // fg.position.x = fg.position.x + ((-player.position.x + this.width / 2) - fg.position.x) * f
      // fg.position.y = fg.position.y + ((-player.position.y + this.height / 2) - fg.position.y) * f
    }
    this.sendPlayerInput()
    return this.scene
  }
}

export default GameStage